#pragma once

#include <cstdint>
#include <vector>
#include <iterator>
#include <optional>
#include <string>

template<typename CharT>
class basic_read_buffer {
public:
	typedef typename std::vector<CharT> Array;
private:
	Array & array;
	std::size_t read_offset;

	virtual std::size_t read_from_source(std::back_insert_iterator<Array> it) =0;
protected:
	void set_read_source(Array source);
public:
	basic_read_buffer(Array & buffer);
	virtual ~basic_read_buffer() {}
	void seek(std::size_t offset);
	std::size_t size() const;

	std::optional<CharT> getc();
	template<typename Tp> bool unpack(Tp & tp);
	template<typename Tc> std::size_t read(Tc & tc, std::size_t n);
	std::optional<std::basic_string<CharT>> read(std::size_t n);
	std::optional<std::basic_string<CharT>> read_until(const std::basic_string<CharT>& pattern);
	std::size_t left_to_read() const;
	template<typename Tp> basic_read_buffer& operator>>(Tp & tp);
	void clear();
};

template<typename CharT>
class basic_write_buffer {
public:
	typedef std::vector<CharT> Array;
private:
	Array & array;
	std::size_t write_offset;

	virtual std::size_t write_to_source(CharT*, std::size_t) =0;
public:
	basic_write_buffer(Array & array);
	virtual ~basic_write_buffer() {}
	void seek(std::size_t offset);
	std::size_t size() const;

	void putc(CharT chr);
	template<typename Tp> void pack(Tp tp);
	void write(const std::basic_string<CharT>& data);
	template<typename Tc> void write(const Tc & tc);
	template<typename Tp> basic_write_buffer& operator<<(const Tp& tp);
	void clear();
	bool flush();
};

template<typename CharT> basic_read_buffer<CharT>::basic_read_buffer(Array & array): array(array) {
	read_offset = 0;
}

template<typename CharT> void basic_read_buffer<CharT>::seek(std::size_t offset) {
	if (offset < array.size()){
		read_offset =  offset;
	}
}

template<typename CharT> std::size_t basic_read_buffer<CharT>::size() const {
	return array.size();
}

template<typename CharT> std::optional<CharT> basic_read_buffer<CharT>::getc() {
	if(read_offset == array.size()){
		std::size_t read = read_from_source(std::back_inserter(array));
		if (read == 0) {
			return {};
		}
	}
	return array[read_offset++];
}

template<typename CharT> template<typename Tp> bool basic_read_buffer<CharT>::unpack(Tp & tp) {
	for(std::size_t i=0; i < sizeof(Tp); ++i) {
		auto chr = getc();
		if(chr) {
			reinterpret_cast<CharT*>(&tp)[i] = *chr;
		} else {
			return false;
		}
	}
	return true;
}

template<typename CharT>
std::optional<std::basic_string<CharT>> basic_read_buffer<CharT>::read(std::size_t n) {
	std::basic_string<CharT> ret;
	for(std::size_t i=0; i < n; ++i) {
		auto chr = getc();
		if(chr) {
			ret += *chr;
		} else {
			return {};
		}
	}
	return ret;
}

template<typename CharT>
template<typename Tc> std::size_t basic_read_buffer<CharT>::read(Tc & tc, std::size_t n) {
	auto it = std::back_inserter(tc);
	for(std::size_t i=0; i < n; ++i) {
		auto chr = getc();
		if(chr) {
			it = *chr;
		} else {
			return i;
		}
	}
	return n;
}

template<typename CharT>
std::optional<std::basic_string<CharT>> basic_read_buffer<CharT>::read_until(const std::basic_string<CharT>& pattern) {
	std::basic_string<CharT> ret;
	std::size_t it = 0;
	for(;;) {
		auto chr = getc();
		if(!chr) {
			return {};
		}
		if(*chr == pattern[it]) {
			it++;
			if(it == pattern.length()) {
				ret += *chr;
				return ret;
			}
		} else {
			it = 0;
		}
		ret += *chr;
	}
}

template<typename CharT>
std::size_t basic_read_buffer<CharT>::left_to_read() const {
	return array.size() - read_offset;
}

template<typename CharT>
template<typename Tp> basic_read_buffer<CharT>& basic_read_buffer<CharT>::operator>>(Tp & tp) {
	if constexpr(std::is_same<Tp, std::basic_string<CharT>>::value) {
		tp = read_until(" ");
	} else {
		unpack(tp);
	}
	return *this;
}

template<typename CharT>
void basic_read_buffer<CharT>::clear() {
	array.clear();
	read_offset = 0;
}
template<typename CharT>
void basic_read_buffer<CharT>::set_read_source(Array source) {
	array = std::move(source);
}

template<typename CharT> basic_write_buffer<CharT>::basic_write_buffer(Array & array): array(array) {
	write_offset = 0;
}
template<typename CharT> void basic_write_buffer<CharT>::seek(std::size_t offset) {
	if(offset < array.size()) {
		write_offset = offset;
	}
}
template<typename CharT> std::size_t basic_write_buffer<CharT>::size() const {
	return array.size();
}

template<typename CharT> void basic_write_buffer<CharT>::putc(CharT chr) {
	if(write_offset == array.size()) {
		array.push_back(chr);
		write_offset++;
	} else {
		array[write_offset++] = chr;
	}
}
template<typename CharT> template<typename Tp> void basic_write_buffer<CharT>::pack(Tp tp) {
	for(std::size_t i=0; i < sizeof(Tp); ++i) {
		putc(reinterpret_cast<CharT*>(&tp)[i]);
	}
}
template<typename CharT> void basic_write_buffer<CharT>::write(const std::basic_string<CharT>& data) {
	for(const auto &i : data) {
		putc(i);
	}
}
template<typename CharT>
template<typename Tc> void basic_write_buffer<CharT>::write(const Tc & tc) {
	for(const auto &i : tc) {
		putc(i);
	}
}
template<typename CharT> void basic_write_buffer<CharT>::clear() {
	write_offset = 0;
	array.clear();
}
template<typename CharT> bool basic_write_buffer<CharT>::flush() {
	long long to_write = array.size();
	std::size_t written = 0;
	while(to_write > 0) {
		std::size_t bytes_written = write_to_source(&array.data()[written], to_write);
		if(bytes_written == 0) {
			return false;
		}
		written += bytes_written;
		to_write -= bytes_written;
	}
	return true;
}
template<typename CharT> 
template<typename Tp> basic_write_buffer<CharT>& basic_write_buffer<CharT>::operator<<(const Tp& tp) {
	if constexpr(std::is_same<Tp, std::basic_string<CharT>>::value) {
		write(tp);
	} else {
		pack(tp);
	}
	return *this;
}
