#pragma once
#ifndef __NET__
#define __NET__

#ifdef __linux__ 
#else
#define _WINSOCKAPI_
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <windows.h>
	#pragma comment (lib, "Ws2_32.lib")
#endif

#include "netlib/tcp_server.hpp"
#include "netlib/tcp_client.hpp"

namespace Net{
	inline void InitNetworking() {

#if defined(_WIN32) || defined(_WIN64)
		WSADATA wsaData;
		if(WSAStartup(MAKEWORD(2,2), &wsaData) != 0){
			WSACleanup();
			throw std::runtime_error("WSAStartup error");
		}
#endif

	}

	inline void DeInitNetworking() {

#if defined(_WIN32) || defined(_WIN64)
		WSACleanup();
#endif

	}

}

#endif
