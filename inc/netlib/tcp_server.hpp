#pragma once

#include "netlib/endpoint.hpp"
#include "netlib/tcp_conn.hpp"
#include "netlib/tcp_conn_set.hpp"
#include "netlib/taskpool.hpp"

#include <optional>
#include <functional>

namespace Net{
	namespace TCP{
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		class basic_server {
			Endpoint<_IP> local;
			std::shared_ptr<_TaskPool> taskpool;
			void bind() const;
		public:
			typedef Connection<_IP, _Data_Type, _TaskPool> ConnectionType;
			typedef std::unique_ptr<ConnectionType> ConnectionPtr;
			typedef ConnectionSet<ConnectionType> ConnectionContainer;
			typedef std::optional<ConnectionPtr> ConnOpt;
            typedef _TaskPool TaskPool;
            typedef std::shared_ptr<_TaskPool> TaskPoolPtr;

			template<typename... _IP_Init> basic_server(_IP_Init&&... IP_Init);
			template<typename... _IP_Init> basic_server(std::shared_ptr<_TaskPool> taskpool, _IP_Init&&... IP_Init);
			~basic_server();

			bool listen(std::size_t n = 10) const;
			std::optional<ConnectionPtr> accept() const;
			template<typename _Handler>
			void async_accept(const _Handler & Handler) const;
			std::shared_ptr<_TaskPool> get_taskpool() const { return taskpool; }
		};

		using Server = basic_server<Net::IP::V4, char, Task::taskpool>;

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		template<typename _Handler>
		void basic_server<_IP, _Data_Type, _TaskPool>::async_accept(const _Handler & Handler) const {
			taskpool->template add_task<
				Task::AsyncTask<
					std::function<ConnOpt()>, 
					std::function<void(ConnOpt)>
				>
			>([this](){ return this->accept(); }, Handler);
		}

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		template<typename... _IP_Init> basic_server<_IP, _Data_Type, _TaskPool>::basic_server(_IP_Init&&... IP_Init): local(std::forward<_IP_Init>(IP_Init)...), taskpool(std::make_shared<_TaskPool>()) {
			bind();
		}

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		template<typename... _IP_Init> basic_server<_IP, _Data_Type, _TaskPool>::basic_server(std::shared_ptr<_TaskPool> taskpool, _IP_Init&&... IP_Init): local(std::forward<_IP_Init>(IP_Init)...), taskpool(taskpool) {
			bind();
		}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		basic_server<_IP, _Data_Type, _TaskPool>::~basic_server() {
#ifdef __linux__
			if(local.sockfd >= 0) {
				::close(local.sockfd);
			}
#else
			if(local.sockfd != INVALID_SOCKET) {
				::closesocket(local.sockfd);
			}
#endif
		}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		void basic_server<_IP, _Data_Type, _TaskPool>::bind() const {
			auto result = ::bind(local.sockfd, reinterpret_cast<const sockaddr*>(&local.IP.addr_struct), sizeof(local.IP.addr_struct));
#ifdef __linux__
			if(result < 0) {
				throw std::runtime_error("bind error");
			}
#else
			if(result == SOCKET_ERROR) {
				throw std::runtime_error("bind error");
			}
#endif
		}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		bool basic_server<_IP, _Data_Type, _TaskPool>::listen(std::size_t n) const {
			auto result = ::listen(local.sockfd, n);
#ifdef __linux__
			if(result != 0) {
				return false;
			}
#else
			if(result == SOCKET_ERROR) {
				return false;
			}
#endif
			return true;
		}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		std::optional<std::unique_ptr<Connection<_IP, _Data_Type, _TaskPool>>> basic_server<_IP, _Data_Type, _TaskPool>::accept() const {
			struct sockaddr_in remote;
#ifdef __linux__
			unsigned int rlen = sizeof(sockaddr_in);
			int sockfd;
#else
			int rlen = sizeof(sockaddr_in);
			SOCKET sockfd;
#endif
			memset(&remote, 0, rlen);
			sockfd = ::accept(local.sockfd, reinterpret_cast<sockaddr*>(&remote), &rlen);
#ifdef __linux__
			if(sockfd < 0) {
#else
			if(sockfd == INVALID_SOCKET) {
#endif
				return {};
			} else {
				return std::make_unique<Connection<_IP, _Data_Type, _TaskPool>>(sockfd, remote, taskpool);
			}
		}
	}
}
