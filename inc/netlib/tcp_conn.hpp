#pragma once

#include "netlib/endpoint.hpp"
#include "netlib/buffer.hpp"

#include <utility>
#include <memory>

#ifdef __linux__
#include <unistd.h>
#include <fcntl.h>
#endif

namespace Net{
	namespace TCP{
		template<typename _Data_Type>
		class Packet;

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		class Connection {
			Endpoint<_IP> remote;
			std::shared_ptr<_TaskPool> taskpool;

			class TCPOstream :public basic_write_buffer<_Data_Type> {
				using Base = basic_write_buffer<_Data_Type>;
				Connection& tcp_conn;
				std::vector<_Data_Type> buffer;

				std::size_t write_to_source(_Data_Type* ptr, std::size_t n) override {
					return tcp_conn.send(ptr, n);
				}
			public:
				TCPOstream(Connection& tcp_conn): Base(buffer), tcp_conn(tcp_conn) {}
			};

			class TCPIstream :public basic_read_buffer<_Data_Type> {
				using Base = basic_read_buffer<_Data_Type>;
				Connection& tcp_conn;
				std::vector<_Data_Type> buffer;

				std::size_t read_from_source(std::back_insert_iterator<typename Base::Array> it) override {
					const std::size_t buffer_size = 0x1000;
					_Data_Type buffer[buffer_size];
					std::size_t read = tcp_conn.recv(buffer, buffer_size);
					std::copy(&buffer[0], &buffer[read], it);
					return read;
				}
			public:
				TCPIstream(Connection& tcp_conn): Base(buffer), tcp_conn(tcp_conn) {}
			};

		public:
#ifdef __linux__
			Connection(int sockfd,const sockaddr_in & remote_addr,const std::shared_ptr<_TaskPool> & taskpool)
#else
			Connection(SOCKET sockfd,const sockaddr_in & remote_addr,const std::shared_ptr<_TaskPool> & taskpool)
#endif
			: remote(sockfd, remote_addr), taskpool(taskpool), ostream(*this), istream(*this) {}

			Connection(const Endpoint<_IP>& remote, std::shared_ptr<_TaskPool> taskpool): remote(remote), taskpool(taskpool), ostream(*this), istream(*this) {}

			~Connection();

			std::size_t recv(_Data_Type* ptr, std::size_t n);
			std::size_t send(_Data_Type* ptr, std::size_t n);

			TCPOstream ostream;
			TCPIstream istream;

			typedef Packet<_Data_Type> PacketType;

			std::optional<PacketType> recv_packet();
			void send_packet(const PacketType& packet);

			std::shared_ptr<_TaskPool> get_taskpool() const { return taskpool; }
            bool set_blocking(bool blocking);
		};

		template<typename _Data_Type>
		class Packet :public basic_read_buffer<_Data_Type>, public basic_write_buffer<_Data_Type> {
			using base_reader = basic_read_buffer<_Data_Type>;
			using base_writer = basic_write_buffer<_Data_Type>;
			using Array = std::vector<_Data_Type>;
			Array buffer;

			std::size_t write_to_source(_Data_Type* ptr, std::size_t n) override { return 0; }
			std::size_t read_from_source(std::back_insert_iterator<typename base_reader::Array> it) override { return 0; }
			void flush();
		public:
			Packet();
			Packet(Array data);
			const Array& get_buffer() const; 
		};

		template<typename _Data_Type> 
		Packet<_Data_Type>::Packet(): base_reader(buffer), base_writer(buffer) {}
		template<typename _Data_Type> 
		Packet<_Data_Type>::Packet(Array data): base_reader(buffer), base_writer(buffer), buffer(std::move(data)) {}
		template<typename _Data_Type>
		const std::vector<_Data_Type>& Packet<_Data_Type>::get_buffer() const {
			return buffer;
		}

		template<typename _IP, typename _Data_Type, typename _TaskPool>
		std::optional<Packet<_Data_Type>> Connection<_IP, _Data_Type, _TaskPool>::recv_packet() {
			uint32_t packet_size;
			if(istream.unpack(packet_size)) {
				std::vector<_Data_Type> data;
				if(istream.recv(data, packet_size) == packet_size) {
					return PacketType(data);
				}
			}
			return {};
		}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		void Connection<_IP, _Data_Type, _TaskPool>::send_packet(const Packet<_Data_Type>& packet) {
			const std::vector<_Data_Type>& buffer = packet.get_buffer();
			uint32_t packet_size = buffer.size();
			ostream.pack(packet_size);
			ostream.write(buffer);
			ostream.flush();
		}

		template<typename _IP, typename _Data_Type, typename _TaskPool>
        bool Connection<_IP, _Data_Type, _TaskPool>::set_blocking(bool blocking) {
#ifdef __linux__
            int flags = fcntl(remote.sockfd, F_GETFL, 0);
            if(flags == -1) {
                return false;
            }
            flags = blocking ? (flags & ~O_NONBLOCK ) : (flags | O_NONBLOCK);
            return fcntl(remote.sockfd, F_SETFL, flags) == 0;
#else
            DWORD mode = blocking ? 0 : 1;
            return ioctlsocket(remote.sockfd, FIONBIO, &mode) == 0;
#endif
        }
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		std::size_t Connection<_IP, _Data_Type, _TaskPool>::recv(_Data_Type* ptr, std::size_t n) {
#ifdef __linux__
			ssize_t recvd;
			std::size_t len = n;
#else
			int recvd;
			int len = static_cast<int>(n);
			if(len < n) {
				throw std::runtime_error("overflow");
			}
#endif
			recvd = ::recv(remote.sockfd, reinterpret_cast<char*>(ptr), len, 0);
#ifdef __linux__
			if(recvd < 0)
#else
			if(recvd == SOCKET_ERROR)
#endif
				return 0;
			return static_cast<std::size_t>(recvd);
		}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		std::size_t Connection<_IP, _Data_Type, _TaskPool>::send(_Data_Type* ptr, std::size_t n) {
#ifdef __linux__
			ssize_t written;
			std::size_t len = n;
#else
			int written;
			int len = static_cast<int>(n);
			if(len < n) {
				throw std::runtime_error("overflow");
			}
#endif
			written = ::send(remote.sockfd, reinterpret_cast<char*>(ptr), len, 0);
#ifdef __linux__
			if(written < 0)
#else
			if(written == SOCKET_ERROR)
#endif
				return 0;
			return static_cast<std::size_t>(written);
		}
		template<typename _IP, typename _Data_Type, typename _TaskPool>
		Connection<_IP, _Data_Type, _TaskPool>::~Connection() {
#ifdef __linux__
			::close(remote.sockfd);
#else
			::closesocket(remote.sockfd);
#endif
		}
	}
}
