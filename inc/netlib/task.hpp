#pragma once

#include <tuple>
#include <functional>

namespace Task {
	class Task {
	public:
		virtual void operator()() =0;
		virtual ~Task(){}
	};

	template<typename... _Callback_Init>
	class CallbackList :public Task {
		std::tuple<_Callback_Init...> callbacks_list;
		template<std::size_t i=0>
		void call() {
			std::get<i>(callbacks_list)();
			if constexpr(i < (sizeof...(_Callback_Init) -1)) {
				call<i+1>();
			}
		}
	public:
		CallbackList(_Callback_Init&&... Callback_Init): callbacks_list(std::forward<_Callback_Init>(Callback_Init)...) {}
		void operator()() override {
			call();
		}
	};

	template<typename _Request, typename _Handler>
	class AsyncTask :public Task{
		_Request Request;
		_Handler Handler;
	public:
		AsyncTask(const _Request& Request, const _Handler& Handler): Request(Request), Handler(Handler) {}
		void operator()() override {
			Handler(Request());
		}
	};
}