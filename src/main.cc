#include <iostream>
#include <chrono>

#include "netlib/netlib.hpp"

using namespace std::chrono;

int main() {
	Net::InitNetworking();

	Net::TCP::Server srv{"0.0.0.0", 1337};
	srv.listen();
	auto conn_opt = srv.accept();
	if(conn_opt) {
		// connection_accepted
		auto conn = std::move(*conn_opt);
		conn->ostream.write("HTTP/1.1 200 OK\r\nContent-Type: TEXT/HTML\r\n\r\n<html><body><h1>It Works</h1></body></html>\r\n");
		conn->ostream.flush();
	}
}

/*


	*/
