#include "netlib/buffer.hpp"
#include <cassert>

class reader :public basic_read_buffer<char> {
	std::vector<char> buffer;
	std::size_t read_from_source(std::back_insert_iterator<typename basic_read_buffer<char>::Array> it) override {
		std::string s = "qwerty\xaa\xbb\xcc\xdd";
		std::copy(s.begin(),s.end(),it);
		return s.length();
	}
public:
	reader(): basic_read_buffer<char>(buffer) {}
};
std::string out;
class writer :public basic_write_buffer<char> {
	std::size_t write_to_source(char* ptr, std::size_t n) override {
		out = std::string(ptr);
		return n;
	}
public:
	std::vector<char> buffer;
	writer(): basic_write_buffer<char>(buffer) {}
};

int main() {
	reader r;
	puts("test 1");assert(*r.getc() == 'q');
	puts("test 2");assert(r.read(2) == "we");
	puts("test 3");assert(r.read_until("ty") == "rty");
	int a;
	r.unpack(a);
	puts("test 4");assert(a == 0xddccbbaa);

	writer w;
	w.putc('a');
	w.write("123");
	int d = 0x44434241;
	w.pack(d);
	w.flush();
	auto output = std::vector<char>{'a','1','2','3', '\x00','A','B','C','D'};
	puts("test 5");assert(w.buffer == output);
	return 0;
}